# Pancreatitis
Pancreatitis is inflammation in the pancreas. The pancreas is a long, flat gland that sits tucked behind the stomach in the upper abdomen.
The pancreas produces enzymes that help digestion and hormones that help regulate the way your body processes sugar (glucose).
Pancreatitis can occur as acute pancreatitis — meaning it appears suddenly and lasts for days. Or pancreatitis can occur as chronic pancreatitis, which is pancreatitis that occurs over many years.
Mild cases of pancreatitis may go away without treatment, but severe cases can cause life-threatening complications.
Symptoms	
Signs and symptoms of pancreatitis may vary, depending on which type you experience.
Acute pancreatitis signs and symptoms include:
•	Upper abdominal pain
•	Abdominal pain that radiates to your back
•	Abdominal pain that feels worse after eating
•	Fever
•	Rapid pulse
•	Nausea
•	Vomiting
•	Tenderness when touching the abdomen
Chronic pancreatitis signs and symptoms include:
•	Upper abdominal pain
•	Losing weight without trying
•	Oily, smelly stools (steatorrhea)
When to see a doctor
Make an appointment with your doctor if you have persistent abdominal pain. Seek immediate medical help if your abdominal pain is so severe that you can't sit still or find a position that makes you more comfortable.
Causes
Pancreatitis occurs when digestive enzymes become activated while still in the pancreas, irritating the cells of your pancreas and causing inflammation.
With repeated bouts of acute pancreatitis, damage to the pancreas can occur and lead to chronic pancreatitis. Scar tissue may form in the pancreas, causing loss of function. A poorly functioning pancreas can cause digestion problems and diabetes.
Conditions that can lead to pancreatitis include:
•	Alcoholism
•	Gallstones
•	Abdominal surgery
•	Certain medications
•	Cigarette smoking
•	Cystic fibrosis
•	Family history of pancreatitis
•	High calcium levels in the blood (hypercalcemia), which may be caused by an overactive parathyroid gland (hyperparathyroidism)
•	High triglyceride levels in the blood (hypertriglyceridemia)
•	Infection
•	Injury to the abdomen
•	Pancreatic cancer
Endoscopic retrograde cholangiopancreatography (ERCP), a procedure used to treat gallstones, also can lead to pancreatitis.
Sometimes, a cause for pancreatitis is never found.
Complications
Pancreatitis can cause serious complications, including:
•	Pseudocyst. Acute pancreatitis can cause fluid and debris to collect in cystlike pockets in your pancreas. A large pseudocyst that ruptures can cause complications such as internal bleeding and infection.
•	Infection. Acute pancreatitis can make your pancreas vulnerable to bacteria and infection. Pancreatic infections are serious and require intensive treatment, such as surgery to remove the infected tissue.
•	Kidney failure. Acute pancreatitis may cause kidney failure, which can be treated with dialysis if the kidney failure is severe and persistent.
•	Breathing problems. Acute pancreatitis can cause chemical changes in your body that affect your lung function, causing the level of oxygen in your blood to fall to dangerously low levels.
•	Diabetes. Damage to insulin-producing cells in your pancreas from chronic pancreatitis can lead to diabetes, a disease that affects the way your body uses blood sugar.
•	Malnutrition. Both acute and chronic pancreatitis can cause your pancreas to produce fewer of the enzymes that are needed to break down and process nutrients from the food you eat. This can lead to malnutrition, diarrhea and weight loss, even though you may be eating the same foods or the same amount of food.
•	Pancreatic cancer. Long-standing inflammation in your pancreas caused by chronic pancreatitis is a risk factor for developing pancreatic cancer.

Visit [this](https://gitlab.com/ShashankPatel/pancreatitis) website and book an appointment if you have any doubts.



